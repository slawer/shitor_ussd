
ActiveRecord::Base.establish_connection(
    :adapter  => "postgresql",
    :host     => "localhost",
    :port     => "4044",
    :username => "sylvanus",
    :password => "password1234",
    :database => "sylvanus_ussd"
)




class UssdRequest < ActiveRecord::Base
end


class Tracker < ActiveRecord::Base
  def self.find_previous_session(mobile_number)
    resultset = where(mobile_number: mobile_number, previous_tracker: false).order('id desc').limit(2)
    puts "RESULT SET FOR PREVIOUS SESSION: #{resultset.last.inspect}"
    resultset.last
  end


  def self.find_previous_three_session(mobile_number)
    resultset = where(mobile_number: mobile_number, previous_tracker: false).order('id desc').limit(3)
    puts "RESULT SET FOR PREVIOUS SESSION: #{resultset.last.inspect}"
    resultset.last
  end


  def self.recent_previous_session(mobile_number)
    resultset = where(mobile_number: mobile_number, previous_tracker: false).order('id desc').limit(2)
    puts "RESULT SET FOR RECENT PREVIOUS SESSION: #{resultset.first.inspect}"
    resultset.first
  end

  def self.new_tracker(session_id)
    where(session_id: session_id, previous_tracker: true).order('id desc').first
  end
end


class TrackerLog < ActiveRecord::Base
end


class BackTracker < ActiveRecord::Base
end



class SessionTracker < ActiveRecord::Base
end

class Contact < ActiveRecord::Base
end

class ContactFinal < ActiveRecord::Base
end


# class More < ActiveRecord::Base
# end
#
# class TempRequest < ActiveRecord::Base
# end