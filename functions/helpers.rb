#log incomming session
def ussd_logger(session_id, mobile_number, request_body, ussd_body, table = UssdRequest)
  table.create(
      session_id: session_id,
      mobile_number: mobile_number,
      body: request_body,
      ussd_body: ussd_body
  )
end


#to continue a session
def continue_session(s_params, body)
  s_params["ussd_body"] = body
  s_params["msg_type"] = '1'
  s_params.to_json

end


#to expire a session
def expire_session(s_params, body = "Your session has ended")
  trackers = Tracker.where(mobile_number: s_params['msisdn'])
  trackers.each {|tracker| tracker.destroy}

  s_params["ussd_body"] = body
  s_params["msg_type"] = '2'
  s_params.to_json
end


def tracker_handler(ussd_body, function, tracker, msg_type, page = "1")

  t = Tracker.create(
      function: function,
      page: page,
      ussd_body: ussd_body,
      mobile_number: tracker.mobile_number,
      session_id: tracker.session_id,
      msg_type: msg_type
  )

  conflicting_row = Tracker.find_previous_session(tracker.mobile_number)
  #delete it
  puts "CONFLICTION ROW: #{conflicting_row.inspect}"
  conflicting_row.destroy if conflicting_row.ussd_body == t.ussd_body && conflicting_row.function == t.function && conflicting_row.page == t.page && t.ussd_body != "*" #last aspect to handle back


  tracker.session_id = nil
  tracker.save #this is the previous handler
end

def get_offset(mobile_number)
  _count = Tracker.where(mobile_number: mobile_number, ussd_body: "02").count()

  if _count == 0
    return "4"
  elsif _count == 1
    return "8"
  elsif _count == 2
    return "12"
  end
end


def contact_offset(offset,mobile_number)
#offsets 0,6,12,18
  @list = ContactFinal.where(user_id: mobile_number,status: true).order("id desc").limit(4).offset(offset)
  @total_count = ContactFinal.where(user_id: mobile_number,status: true).count()

  pre_pages = @total_count / 4
  rem = @total_count % 4

  if rem != 0
    pre_pages = pre_pages + 1
  end

  _count = 0
  _string = "Select Contact\n"

  @list.each do |a_row|
    _count = _count + 1
    _string = _string + "#{_count}. " + "#{a_row.f_name.titleize} #{a_row.l_name.titleize}" + "\n"

  end

  if offset != "12"
    _string = _string + MORE + ". More" + "\n"
  end

  _string = _string + BACK + ". Back\n01. Main Menu"

  return _string
end


#handling the back my way
def back_helper(mobile_number, tracker, arguments)

  the_count = Tracker.where(mobile_number: mobile_number).count()

  if the_count.to_i < 2 #display main menu
    menu(arguments)
  else
    last_two = Tracker.find_previous_session(mobile_number)
    last_one = Tracker.recent_previous_session(mobile_number)


    ussd_body_prev = last_one.ussd_body

    the_function = last_two.function
    the_page = last_two.page

    if the_function == MENU && ussd_body_prev == BACK #to handle continous back
      menu(arguments)
    else

      #take last 3's function and page , then take last 2's body
      last_three = Tracker.find_previous_three_session(mobile_number)

      the_function = last_three.function
      the_page = last_three.page
      ussd_body_prev = last_two.ussd_body


      #now delete the last record in the tracker and create a new tracker from here on
      tracker_del = Tracker.where(mobile_number: mobile_number).order('id desc').first
      tracker_del.destroy

      new_tracker = Tracker.find_previous_session(mobile_number)

      arguments['ussd_body'] = ussd_body_prev
      functions_manager(new_tracker, arguments)
    end
  end
end


