
#manages the function for the menus

def functions_manager(tracker, arguments)
  s_params = arguments['s_params']

  ussd_body = arguments['ussd_body']
  mobile_number = arguments['mobile_number']
  puts "FUNCTION IS:"
  puts tracker.function
  puts "PAGE IS:"
  puts tracker.page
  puts "USSD_BODY IS:"
  puts ussd_body


  if ussd_body == BACK #back 1 page
    back_helper(mobile_number, tracker, arguments)

  elsif ussd_body == '01' #back to home page
    #delete trackers
    trackers = Tracker.where(mobile_number: mobile_number)
    trackers.each {|tracker| tracker.destroy}
    menu(arguments)

  elsif tracker.function==MENU && ussd_body=="1"
    register_func(tracker,arguments)

  elsif tracker.function==MENU && ussd_body=="2"
    view_contacts(tracker,arguments)

  elsif tracker.function==MENU && ussd_body=="3"
    edit_contacts(tracker,arguments)

  elsif tracker.function==MENU && ussd_body=="4"
    delete_contacts(tracker,arguments)

  elsif tracker.function==MENU && ussd_body=="5"
    expire_session(s_params,"Thank you for using SHITOR PB...")

  elsif tracker.function==REG
    register_func(tracker,arguments)

  elsif tracker.function==VIEW
    view_contacts(tracker,arguments)

  elsif tracker.function==EDIT
    edit_contacts(tracker,arguments)

  elsif tracker.function==DEL
    delete_contacts(tracker,arguments)

  else
    expire_session(s_params,"Wrong input chosen.")
  end
end
