# menu functions


def menu(arguments)
  #check which page to show user
  #subscriber = has_subscribed?(mobile_number, user_table)

  mobile_number = arguments['mobile_number']
  session_id = arguments['session_id']
  ussd_body = arguments['ussd_body']
  msg_type = arguments['msg_type']
  s_params = arguments['s_params']

  t_old = Tracker.find_by(session_id: session_id)
  if t_old
    tracker_handler(ussd_body, MENU, t_old, msg_type)
  else
    t = Tracker.create(function: MENU, page: '1', ussd_body: ussd_body, mobile_number: mobile_number, session_id: session_id, msg_type: msg_type)
  end

  continue_session(s_params, MAIN_MENU_BODY)

end

def register_func(tracker, arguments)
  mobile_number = arguments['mobile_number']
  session_id = arguments['session_id']
  ussd_body = arguments['ussd_body']
  msg_type = arguments['msg_type']
  s_params = arguments['s_params']

  if tracker.function != REG
    tracker_handler(ussd_body, REG, tracker, msg_type) #this saves to the tracker db always
    continue_session(s_params, F_NAME)

  elsif tracker.page == "1"
    #Contact.create(mobile_number: mobile_number, f_name: ussd_body)
    tracker_handler(ussd_body, REG, tracker, msg_type, '2') #this saves to the tracker db always
    continue_session(s_params, L_NAME)

  elsif tracker.page == "2"
    tracker_handler(ussd_body, REG, tracker, msg_type, '3') #this saves to the tracker db always
    continue_session(s_params, PHONE)
  elsif tracker.page == "3"
    puts "=============================================================================================="
    puts "====================================ADD SUMMARY DETAILS===================================="

    _selectedFname = Tracker.where(mobile_number: mobile_number, page: "2", function: REG).order('id desc').pluck(:ussd_body)[0]
    _selectedLname = Tracker.where(mobile_number: mobile_number, page: "3", function: REG).order('id desc').pluck(:ussd_body)[0]
    #_selectedPhone = Tracker.where(mobile_number: mobile_number, page: "4",function: REG).order('id desc').pluck(:ussd_body)[0]

    #puts "THE count: #{_selected}"
    puts "THE firstname: #{_selectedFname}"
    puts "THE lastname: #{_selectedLname}"
    puts "THE phone number: #{ussd_body}"
    puts "====================================ADD SUMMARY DETAILS===================================="
    puts "=============================================================================================="
    tracker_handler(ussd_body, REG, tracker, msg_type, '4') #this saves to the tracker db always
    continue_session(s_params, "Summary Page\nName: #{_selectedFname} #{_selectedLname}\nPhone: #{ussd_body}\n" + CONF)
  elsif tracker.page == "4"
    tracker_handler(ussd_body, REG, tracker, msg_type, '5') #this saves to the tracker db always

    puts "=============================================================================================="
    puts "====================================ADD SUMMARY DETAILS===================================="

    _selectedFname = Tracker.where(mobile_number: mobile_number, page: "2", function: REG).order('id desc').pluck(:ussd_body)[0]
    _selectedLname = Tracker.where(mobile_number: mobile_number, page: "3", function: REG).order('id desc').pluck(:ussd_body)[0]
    _selectedPhone = Tracker.where(mobile_number: mobile_number, page: "4", function: REG).order('id desc').pluck(:ussd_body)[0]

    #puts "THE count: #{_selected}"
    puts "THE firstname: #{_selectedFname}"
    puts "THE lastname: #{_selectedLname}"
    puts "THE phone number: #{_selectedPhone}"
    puts "====================================ADD SUMMARY DETAILS===================================="
    puts "=============================================================================================="

    if ussd_body == "1"
      puts @cont = ContactFinal.create!(phone_number: "#{_selectedPhone}", f_name: "#{_selectedFname.downcase}", l_name: "#{_selectedLname.downcase}", user_id: "#{mobile_number}", status: true)

      puts "====================================ADDED CONTACT DETAILS===================================="
      puts "====================================Saved Contact ::::#{@cont.inspect}======================================="
      puts "====================================ADDED CONTACT DETAILS===================================="
      expire_session(s_params, "Thank you for using SHITOR PB...")
    elsif ussd_body == "2"
      expire_session(s_params, "Thank you for using SHITOR PB...")
    else
      expire_session(s_params, "Wrong input selected")
    end
  else
    expire_session(s_params, "Wrong input selected")
  end
end

def view_contacts(tracker, arguments)
  mobile_number = arguments['mobile_number']
  session_id = arguments['session_id']
  ussd_body = arguments['ussd_body']
  msg_type = arguments['msg_type']
  s_params = arguments['s_params']


  tracker_handler(ussd_body, VIEW, tracker, msg_type) #this saves to the tracker db always
  if tracker.function != VIEW


    display = contact_offset("0", mobile_number)

    continue_session(s_params, display)

  elsif ussd_body == MORE #handles the more aspect
    offset = get_offset(mobile_number)
    tracker_handler(ussd_body, VIEW, tracker, msg_type, '10') #this saves to the tracker db always

    display = contact_offset(offset, mobile_number)
    continue_session(s_params, display)


  end
end

def edit_contacts(tracker, arguments)
  mobile_number = arguments['mobile_number']
  session_id = arguments['session_id']
  ussd_body = arguments['ussd_body']
  msg_type = arguments['msg_type']
  s_params = arguments['s_params']


  if tracker.function != EDIT


    tracker_handler(ussd_body, EDIT, tracker, msg_type, '10') #this saves to the tracker db always


    display = contact_offset("0", mobile_number)

    continue_session(s_params, display)

  elsif ussd_body == MORE #handles the more aspect
    offset = get_offset(mobile_number)
    tracker_handler(ussd_body, EDIT, tracker, msg_type, '10') #this saves to the tracker db always

    display = contact_offset(offset, mobile_number)
    continue_session(s_params, display)

  elsif tracker.page == '10' && CONTACT_LIST.include?(ussd_body) #show the next menu
    tracker_handler(ussd_body, EDIT, tracker, msg_type, '2')
    continue_session(s_params, F_NAME)

  elsif tracker.page == '2'
    tracker_handler(ussd_body, EDIT, tracker, msg_type, '3')
    continue_session(s_params, L_NAME)

  elsif tracker.page == '3'
    tracker_handler(ussd_body, EDIT, tracker, msg_type, '4')
    continue_session(s_params, PHONE)

  elsif tracker.page == '4' #summary page comes here
    tracker_handler(ussd_body, EDIT, tracker, msg_type, '5')

    puts "=============================================================================================="
    puts "====================================COUNT NUMBER=========================================================="

    _count = Tracker.where(mobile_number: mobile_number, ussd_body: "02").count

    puts "THE count: #{_count}"
    puts "=====================================COUNT NUMBER=============================================="
    puts "=============================================================================================="

    puts "=============================================================================================="
    puts "====================================SUMMARY DETAILS===================================="

    _selected = Tracker.where(mobile_number: mobile_number, page: "2").order('id desc').pluck(:ussd_body)[0]
    _selectedFname = Tracker.where(mobile_number: mobile_number, page: "3", function: EDIT).order('id desc').pluck(:ussd_body)[0]
    _selectedLname = Tracker.where(mobile_number: mobile_number, page: "4", function: EDIT).order('id desc').pluck(:ussd_body)[0]
    _selectedPhone = Tracker.where(mobile_number: mobile_number, page: "5", function: EDIT).order('id desc').pluck(:ussd_body)[0]

    puts "THE count: #{_selected}"
    puts "THE firstname: #{_selectedFname}"
    puts "THE lastname: #{_selectedLname}"
    puts "THE phone number: #{_selectedPhone}"
    puts "====================================SUMMARY DETAILS===================================="
    puts "=============================================================================================="
    summary = ""

    offset = 0
    #id_value = cont[id -1].id

    if _count == 0
      offset = "0"
    elsif _count == 1
      offset = "4"
    elsif _count == 2
      offset = "8"
    elsif _count == 3
      offset = "12"
    end

    @list = ContactFinal.where(user_id: mobile_number,status: true).order("id desc").limit(4).offset(offset)

    puts "Here is the list of contacts #{@list.inspect}"
    _count = 1
    @list.each do |a_row|

      puts "the count is on ::: #{_count}"
      puts _count.class
      puts "the selected is on ::: #{_selected.to_i}"
      puts _selected.to_i.class

      if _count == _selected.to_i
        puts "************************************************************"
        puts "************************************************************"
        puts "#{_selected.to_i}"
        puts "************************************************************"
        puts "************************************************************"

        summary = "Summary:\nName: #{_selectedFname} #{_selectedLname} \nPhone: #{_selectedPhone}\n1. Proceed\n" + BACK + ". Back\n01. Main Menu"

      end
      _count = _count.to_i + 1
    end

    continue_session(s_params, summary)

  elsif tracker.page == '5'

    tracker_handler(ussd_body, EDIT, tracker, msg_type, '6')
    _contact_id = ""
    _count = Tracker.where(mobile_number: mobile_number, ussd_body: "02").count

    puts "THE count: #{_count}"
    puts "=====================================COUNT NUMBER=============================================="
    puts "=============================================================================================="

    puts "=============================================================================================="
    puts "====================================EDITED CONTACT DETAILS===================================="

    _selected = Tracker.where(mobile_number: mobile_number, page: "2", function: EDIT).order('id desc').pluck(:ussd_body)[0]
    _selectedFname = Tracker.where(mobile_number: mobile_number, page: "3", function: EDIT).order('id desc').pluck(:ussd_body)[0]
    _selectedLname = Tracker.where(mobile_number: mobile_number, page: "4", function: EDIT).order('id desc').pluck(:ussd_body)[0]
    _selectedPhone = Tracker.where(mobile_number: mobile_number, page: "5", function: EDIT).order('id desc').pluck(:ussd_body)[0]

    puts "THE count: #{_selected}"
    puts "THE firstname: #{_selectedFname}"
    puts "THE lastname: #{_selectedLname}"
    puts "THE locality: #{_selectedPhone}"
    puts "====================================EDITED CONTACT DETAILS===================================="
    puts "=============================================================================================="


    offset = 0
    #id_value = cont[id -1].id

    if _count == 0
      offset = "0"
    elsif _count == 1
      offset = "4"
    elsif _count == 2
      offset = "8"
    elsif _count == 3
      offset = "12"
    end

    if ussd_body == "1"


      @list = ContactFinal.where(user_id: mobile_number,status: true).order("id desc").limit(4).offset(offset)

      puts "Here is the list of contacts #{@list.inspect}"
      _count = 1
      @list.each do |a_row|

        puts "the count is on ::: #{_count}"
        puts _count.class
        puts "the selected is on ::: #{_selected.to_i}"
        puts _selected.to_i.class

        if _count == _selected.to_i

          _contact_id = "#{a_row.id}"
        end
        _count = _count.to_i + 1
      end

      puts "************************************************************"
      puts "************************************************************"
      puts "This is the contact id::: #{_contact_id}"
      puts "************************************************************"
      puts "************************************************************"

      @contact = ContactFinal.where(:id => "#{_contact_id}", :user_id => mobile_number, :status => true)
      @contact.update_all(f_name: _selectedFname.to_s.downcase, l_name: _selectedLname.to_s.downcase, phone_number: _selectedPhone.to_s.downcase)

      puts "************************************************************"
      puts "************************************************************"
      puts "************************************************************"
      puts "***************Updated Successfully**********************"
      puts "The updated contact:::: #{@contact.inspect}"
      puts "***************Updated Successfully*************************"
      puts "************************************************************"
      puts "************************************************************"
      puts "************************************************************"
      expire_session(s_params, "Successfully updated!!!")

    else
      expire_session(s_params, "Failed update!!!")
    end
    #continue_session(s_params, LOC_NAME)
    # @contacts.each do |contact|
    #   cont=cont + 1
    #   display << "#{cont}. #{contact.f_name} #{contact.l_name}\n"
    # end
    # display << "00. Back\n01. Menu\n"
    # if LIMIT < @contacts.size
    #   display << "02. More"
    # end
    # #index_function(VIEW, mobile_number, 1, session_id)
    # continue_session(s_params, display)
  end
end

def delete_contacts(tracker, arguments)
  mobile_number = arguments['mobile_number']
  session_id = arguments['session_id']
  ussd_body = arguments['ussd_body']
  msg_type = arguments['msg_type']
  s_params = arguments['s_params']


  if tracker.function != DEL

    tracker_handler(ussd_body, DEL, tracker, msg_type, '10') #this saves to the tracker db always


    display = contact_offset("0", mobile_number)

    continue_session(s_params, display)

  elsif ussd_body == MORE #handles the more aspect
    offset = get_offset(mobile_number)
    tracker_handler(ussd_body, DEL, tracker, msg_type, '10') #this saves to the tracker db always

    display = contact_offset(offset, mobile_number)
    continue_session(s_params, display)

  elsif tracker.page == '10' && CONTACT_LIST.include?(ussd_body) #show the next menu
    tracker_handler(ussd_body, DEL, tracker, msg_type, '2')
    continue_session(s_params, "Proceed\n1. Yes\n2. No")

  elsif tracker.page == '2'

    tracker_handler(ussd_body, DEL, tracker, msg_type, '3')
    _contact_id = ""
    _count = Tracker.where(mobile_number: mobile_number, ussd_body: "02").count

    puts "THE count: #{_count}"
    puts "=====================================COUNT NUMBER=============================================="
    puts "=============================================================================================="

    puts "=============================================================================================="
    puts "====================================DELETE CONTACT DETAILS===================================="

    _selected = Tracker.where(mobile_number: mobile_number, page: "2",function: DEL).order('id desc').pluck(:ussd_body)[0]

    puts "THE selected id: #{_selected}"

    puts "====================================DELETE CONTACT DETAILS===================================="
    puts "=============================================================================================="


    offset = 0
    #id_value = cont[id -1].id

    if _count == 0
      offset = "0"
    elsif _count == 1
      offset = "4"
    elsif _count == 2
      offset = "8"
    elsif _count == 3
      offset = "12"
    end

    if ussd_body == "1"


      @list = ContactFinal.where(user_id: mobile_number,status: true).order("id desc").limit(4).offset(offset)

      puts "Here is the list of contacts #{@list.inspect}"
      _count = 1
      @list.each do |a_row|

        puts "the count is on ::: #{_count}"
        puts _count.class
        puts "the selected is on ::: #{_selected.to_i}"
        puts _selected.to_i.class

        if _count == _selected.to_i

          _contact_id = "#{a_row.id}"
        end
        _count = _count.to_i + 1
      end

      puts "************************************************************"
      puts "************************************************************"
      puts "This is the contact id::: #{_contact_id}"
      puts "************************************************************"
      puts "************************************************************"

      @contact = ContactFinal.where(:id => "#{_contact_id}", :user_id => mobile_number,:status => true)
      @contact.update_all(status: false)

      puts "The deleted contact:::: #{@contact.inspect}"
      puts "The deleted contact:::: #{@contact.inspect}"
      puts "The deleted contact:::: #{@contact.inspect}"
      puts "The deleted contact:::: #{@contact.inspect}"
      puts "The deleted contact:::: #{@contact.inspect}"
      puts "The deleted contact:::: #{@contact.inspect}"
      puts "The deleted contact:::: #{@contact.inspect}"
      puts "The deleted contact:::: #{@contact.inspect}"
      puts "The deleted contact:::: #{@contact.inspect}"
      puts "The deleted contact:::: #{@contact.inspect}"
      expire_session(s_params, "Contact deleted!!!")

    else
      expire_session(s_params, "Failed to delete!!!")
    end
    #continue_session(s_params, LOC_NAME)
    # @contacts.each do |contact|
    #   cont=cont + 1
    #   display << "#{cont}. #{contact.f_name} #{contact.l_name}\n"
    # end
    # display << "00. Back\n01. Menu\n"
    # if LIMIT < @contacts.size
    #   display << "02. More"
    # end
    # #index_function(VIEW, mobile_number, 1, session_id)
    # continue_session(s_params, display)
  end
end
