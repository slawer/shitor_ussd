
def process(json)
  s_params = JSON.parse(json)

  arguments = Hash.new
  arguments['mobile_number'] = s_params['msisdn']
  arguments['session_id'] = s_params['session_id']
  arguments['nw_code'] = s_params['nw_code']
  arguments['ussd_body'] = s_params['ussd_body']
  arguments['msg_type'] = s_params['msg_type']
  arguments['s_params'] = s_params

  msg_type = s_params['msg_type']
  mobile_number =  s_params['msisdn']
  ussd_body = s_params['ussd_body']
  session_id = s_params['session_id']

  #prevent voda from messing up the tracker
  if VODA_BLACKLIST.include?(ussd_body) && s_params['service_code'].blank?
    puts "SESSION ENDED DUE TO BLACKLISTED USSD BODY"
    return continue_session(s_params,"session ended")
  end


  ussd_logger(arguments['session_id'],arguments['mobile_number'],ussd_body,json)


  tracker = Tracker.find_previous_session(mobile_number)
  recent_prev_session = Tracker.recent_previous_session(mobile_number)

  puts "================"
  puts "BEFORE ANYTHING ELSE <3 checking the tracker"
  puts "tracker: #{tracker.inspect}"
  puts "================"

  new_tracker = Tracker.new_tracker(session_id)


  if tracker && msg_type == '0'
    t = Tracker.create(
        function: PREVIOUS_SESSION_FUNC,
        page: '1',
        ussd_body: ussd_body,
        mobile_number: mobile_number,
        session_id: session_id,
        msg_type: msg_type,
        previous_tracker: true
    )
    return continue_session(s_params,PREVIOUS_TRACKER_BODY)



  elsif new_tracker && new_tracker.function == PREVIOUS_SESSION_FUNC && new_tracker.page == '1'
    if ussd_body == '1'
      #delete fake trackers

      sql = "DELETE FROM trackers WHERE mobile_number = '#{mobile_number}' AND previous_tracker = true"
      Tracker.connection.execute(sql)
      ussd_body_prev = recent_prev_session.ussd_body
      tracker.session_id = session_id #update session id
      tracker.save

      arguments['ussd_body'] = ussd_body_prev
      return functions_manager(tracker,arguments)

    elsif ussd_body == '2'
      #destroy trackers and call menu
      sql = "DELETE FROM trackers WHERE mobile_number = '#{mobile_number}'"
      Tracker.connection.execute(sql)
      p "CHOSE NOT TO CONTINUE... TRACKERS DESTROYED..."

      return menu(arguments)
    else
      return expire_session(s_params,'Wrong input chosen.')
    end

  end





























  if msg_type == '0'
    menu(arguments)
  else

    tracker = Tracker.where(mobile_number: mobile_number).order('id desc').first

    #to handle the back
    if ussd_body == BACK
      return back_helper(mobile_number, tracker, arguments)
    end

    functions_manager(tracker, arguments)
  end

end