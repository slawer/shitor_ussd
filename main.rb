require 'sinatra'
require 'sinatra/activerecord'
require 'json'
require './functions/functions'
require './models/model'
require './functions/constants'
require './functions/helpers'
require './functions/manager'
require './functions/process'


post "/" do
  request.body.rewind
  process(request.body.read)
end
