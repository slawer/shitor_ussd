require 'sinatra'
require 'sinatra/activerecord'
require './models/model'

ActiveRecord::Migration.create_table :contact_finals do |t|
  t.string :f_name
  t.string :l_name
  t.string :phone_number
  t.string :user_id
  t.boolean :status
  t.timestamps null: false
end

ActiveRecord::Migration.create_table :contacts do |t|
  t.string :f_name
  t.string :l_name
  t.string :phone_number
  t.string :user_id
  t.boolean :status
  t.timestamps null: false
end

ActiveRecord::Migration.create_table :ussd_requests do |t|
  t.string :session_id
  t.string :mobile_number
  t.string :ussd_body
  t.string :body
  t.timestamps null: false
end


ActiveRecord::Migration.create_table :session_trackers do |t|
  t.string :session_id
  t.integer :function
  t.boolean :previous_tracker
  t.string :page
  t.integer :mobile_number
  t.string :ussd_body
  t.string :msg_type
  t.timestamps null: false
end

ActiveRecord::Migration.create_table :trackers do |t|
  t.string :session_id
  t.string :function
  t.string :page
  t.string :mobile_number
  t.string :ussd_body
  t.string :msg_type
  t.boolean :previous_tracker, :default => false
  t.timestamps null: false
end


ActiveRecord::Migration.create_table :tracker_logs do |t|
  t.string :session_id
  t.string :function
  t.string :page
  t.string :mobile_number
  t.string :ussd_body
  t.string :msg_type
  t.boolean :previous_tracker
  t.timestamps null: false
end



