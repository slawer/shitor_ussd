require 'rubygems'

require 'sinatra'

set :environment, ENV['RACK_ENV'].to_sym

disable :run, :reload

require './main.rb'

run Sinatra::Application